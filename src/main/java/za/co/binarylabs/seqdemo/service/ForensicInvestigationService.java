package za.co.binarylabs.seqdemo.service;

import za.co.binarylabs.seqdemo.model.ForensicInvestigation;
import za.co.binarylabs.seqdemo.model.InvestigationType;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2015/10/14
 * Time: 8:22 AM
 * To change this template use File | Settings | File Templates.
 */
public interface ForensicInvestigationService {

    ForensicInvestigation findInvestigation(String name);

    ForensicInvestigation save(ForensicInvestigation forensicInvestigation);

    List<ForensicInvestigation> findAll();

    InvestigationType save(InvestigationType investigationType);
}
