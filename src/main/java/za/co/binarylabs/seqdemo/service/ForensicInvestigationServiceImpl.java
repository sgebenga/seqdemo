package za.co.binarylabs.seqdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.binarylabs.seqdemo.model.ForensicInvestigation;
import za.co.binarylabs.seqdemo.model.InvestigationType;
import za.co.binarylabs.seqdemo.repository.ForensicInvestigationRepository;
import za.co.binarylabs.seqdemo.repository.InvestigationTypeRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2015/10/14
 * Time: 8:23 AM
 * To change this template use File | Settings | File Templates.
 */
@Component("forensicInvestigationService")
public class ForensicInvestigationServiceImpl implements ForensicInvestigationService {

    @Autowired
    private ForensicInvestigationRepository forensicInvestigationRepository;

    @Autowired
    private InvestigationTypeRepository investigationTypeRepository;

    @Override
    public ForensicInvestigation findInvestigation(String name) {
        return null;
    }

    @Override
    public ForensicInvestigation save(ForensicInvestigation forensicInvestigation) {
        return forensicInvestigationRepository.save(forensicInvestigation);
    }

    @Override
    public InvestigationType save(InvestigationType investigationType) {
        return investigationTypeRepository.save(investigationType);
    }

    @Override
    public List<ForensicInvestigation> findAll() {
        return forensicInvestigationRepository.findAll();
    }
}
