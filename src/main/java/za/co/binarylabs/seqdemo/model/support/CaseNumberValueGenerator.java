package za.co.binarylabs.seqdemo.model.support;

//import org.apache.log4j.spi.LoggerFactory;

import org.hibernate.Session;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.hibernate.tuple.ValueGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.binarylabs.seqdemo.model.ForensicInvestigation;
import za.co.binarylabs.seqdemo.model.InvestigationType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2015/10/14
 * Time: 9:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class CaseNumberValueGenerator implements ValueGenerator<String> {

    Logger LOGGER = LoggerFactory.getLogger(CaseNumberValueGenerator.class);

    @Override
    public String generateValue(Session session, Object o) {
        LOGGER.info("Instance of ForensicInvestigation " + (o instanceof ForensicInvestigation));
        ForensicInvestigation investigation = (ForensicInvestigation) o;
        String prefix = investigation.getInvestigationType().getPrefix();
        Integer suffix = suffix(session, investigation);
        return String.format("%s%s", prefix, suffix);
    }

    private Integer suffix(Session session, final ForensicInvestigation investigation) {
        final Integer suffix = session.doReturningWork(new ReturningWork<Integer>() {
            @Override
            public Integer execute(Connection connection) throws SQLException {
                int id = 0;
                try {
                    PreparedStatement ps = connection
                            .prepareStatement(String.format("SELECT nextval ('%s') as nextval", investigation.getInvestigationType().getSeq()));   //Read default value if not set

                    ResultSet rs = ps.executeQuery();
                    if (rs.next()) {
                        id = rs.getInt("nextval");
                        LOGGER.info("Next Sequence Value " + id);

                    }

                } catch (Exception e) {
                    LOGGER.debug("Exception Getting Sequence Value", e);
                }
                return id;
            }
        });
        /*
        session.doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
                //suffix = 100;
                investigation.setCaseNumber(String.format("%s%s", investigation.getInvestigationType().getPrefix(), 680));
            }
        });
        */
        return suffix;
    }
}
