package za.co.binarylabs.seqdemo.model;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GeneratorType;
import za.co.binarylabs.seqdemo.model.support.CaseNumberValueGenerator;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2015/10/14
 * Time: 8:00 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class ForensicInvestigation {


    private Long id;
    private String caseNumber;
    private String description;
    private InvestigationType investigationType;

    public ForensicInvestigation() {
    }

    public ForensicInvestigation(String description, InvestigationType investigationType) {
        this.description = description;
        this.investigationType = investigationType;
    }

    public ForensicInvestigation(Long id, String description, String caseNumber, InvestigationType investigationType) {
        this.id = id;
        this.description = description;
        this.caseNumber = caseNumber;
        this.investigationType = investigationType;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "case_number")
    @GeneratorType(type = CaseNumberValueGenerator.class, when = GenerationTime.INSERT)
    public String getCaseNumber() {
        return caseNumber;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }

    @ManyToOne
    @JoinColumn(name = "investigation_type", referencedColumnName = "id")
    public InvestigationType getInvestigationType() {
        return investigationType;
    }

    public void setInvestigationType(InvestigationType investigationType) {
        this.investigationType = investigationType;
    }

    @Override
    public String toString() {
        return "ForensicInvestigation{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", caseNumber='" + caseNumber + '\'' +
                ", investigationType=" + investigationType +
                '}';
    }
}
