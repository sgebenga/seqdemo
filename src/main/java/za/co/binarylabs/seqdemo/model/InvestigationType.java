package za.co.binarylabs.seqdemo.model;

import javax.annotation.Generated;
import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2015/10/14
 * Time: 8:02 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class InvestigationType {
    private static final String DEFAULT_SEQ="FORENSICS_DISCOVERY_SEQ";
    private Long id;
    private String name;
    private String prefix;
    private String seq;

    public InvestigationType() {

    }

    public InvestigationType(String name, String prefix) {
        this.name = name;
        this.prefix = prefix;
        this.seq= DEFAULT_SEQ;
    }

    public InvestigationType(String name, String prefix, String seq) {
        this(name, prefix);
        this.seq = seq;
    }

    public InvestigationType(Long id, String name, String prefix, String seq) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.seq = seq;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "prefix")
    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Column(name = "seq")
    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    @Override
    public String toString() {
        return "InvestigationType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", prefix='" + prefix + '\'' +
                ", seq='" + seq + '\'' +
                '}';
    }
}
