package za.co.binarylabs.seqdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import za.co.binarylabs.seqdemo.model.ForensicInvestigation;
import za.co.binarylabs.seqdemo.model.InvestigationType;
import za.co.binarylabs.seqdemo.service.ForensicInvestigationService;

@SpringBootApplication
public class SeqdedmoApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(SeqdedmoApplication.class);

    @Autowired
    ForensicInvestigationService forensicInvestigationService;

    public static void main(String[] args) {
        SpringApplication.run(SeqdedmoApplication.class, args);
        LOGGER.info("application running");
    }

    @Bean
    public org.springframework.boot.CommandLineRunner demo(final ForensicInvestigationService forensicInvestigationService) {
        return new CommandLineRunner() {
            @Override
            public void run(String... strings) throws Exception {
                //save investigation type
                InvestigationType healthType = new InvestigationType("Health", "HE", "FORENSICS_HEALTH_SEQ");
                InvestigationType lifeType = new InvestigationType("Health", "LI", "FORENSICS_LIFE_SEQ");

                //save investigation types
                forensicInvestigationService.save(healthType);
                forensicInvestigationService.save(lifeType);

                // save investigation
                forensicInvestigationService.save(new ForensicInvestigation("A test investigation", healthType));
                forensicInvestigationService.save(new ForensicInvestigation("A second Health investigation", healthType));
                forensicInvestigationService.save(new ForensicInvestigation("A third investigation", healthType));
                for (int i = 1; i < 100; i++) {
                    forensicInvestigationService.save(new ForensicInvestigation(String.format("# %s life investigation", i), lifeType));
                }

                //fetch all investigations
                for (ForensicInvestigation investigation : forensicInvestigationService.findAll()) {
                    LOGGER.info(investigation.toString());
                }
            }
        };
    }

    private static void addEntities() {
        ForensicInvestigation investigation = new ForensicInvestigation();
        investigation.setCaseNumber("test string");

    }
}
