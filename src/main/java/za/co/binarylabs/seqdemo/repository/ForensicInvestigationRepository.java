package za.co.binarylabs.seqdemo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.binarylabs.seqdemo.model.ForensicInvestigation;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2015/10/14
 * Time: 8:20 AM
 * To change this template use File | Settings | File Templates.
 */
//@Repository("forensicInvestigationRepository")
public interface ForensicInvestigationRepository extends JpaRepository<ForensicInvestigation, Long> {
}
