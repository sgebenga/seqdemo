package za.co.binarylabs.seqdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.binarylabs.seqdemo.model.InvestigationType;

/**
 * Created with IntelliJ IDEA.
 * User: khaya
 * Date: 2015/10/14
 * Time: 8:52 AM
 * To change this template use File | Settings | File Templates.
 */
public interface InvestigationTypeRepository extends JpaRepository<InvestigationType,Long> {
}
