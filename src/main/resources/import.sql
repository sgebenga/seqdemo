-- ================= create Health Sequence ==============
CREATE SEQUENCE IF NOT EXISTS FORENSICS_HEALTH_SEQ  START WITH 1 INCREMENT BY 1;

-- ================= create Life Sequence ==============
CREATE SEQUENCE IF NOT EXISTS FORENSICS_LIFE_SEQ START WITH 1 INCREMENT BY 1;

-- ================= create Invest Sequence ==============
CREATE SEQUENCE IF NOT EXISTS FORENSICS_INVEST_SEQ START WITH 1 INCREMENT BY 1;

-- ================= create Discovery Sequence for Defaults ==============
CREATE SEQUENCE IF NOT EXISTS FORENSICS_DISCOVERY_SEQ  START WITH 1 INCREMENT BY 1;